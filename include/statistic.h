#ifndef STATISTIC_H
#define STATISTIC_H

#include "fileMng.h"
#include "calc.h"



class statistic : public fileMng , public calc
{
    public:
        statistic(string path,int columns,vector<string>label,char sign);
        virtual ~statistic();
        void arithmetic(int datatype);
        void geometric(int datatype);
        void harmonic(int datatype);
        void dominant(int datatype);
        void centralMoment(int datatype,double x);
        void median(int datatype);
        void variance(int datatype);
        void standardDeviation(int datatype);
        void quartile(int datatype,int n);
        void kurtosis(int datatype);
        void slant(int datatype);
        void counter(int datatype);
        void tstudent(int datatype,double alpha);
    private:
        double t_studentTab(int df, double alpha);
        int findDf(int df,double tab[][2]);
        int findAlpha(double alpha, double tab[][2]);
        vector<double> splitData(vector<record>values,string str);
        void clearData();
};

#endif // STATISTIC_H
