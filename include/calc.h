#ifndef CALC_H
#define CALC_H
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

struct sortingStruct
{
    inline bool operator() (const double& x, const double& y)
    {
        return (x < y);
    }
};

class calc
{
    public:
        calc();
        virtual ~calc();

    protected:
        double calc_dominant(vector<double>values);
        double calc_median(vector<double>values);
        double calc_arithmetic(vector<double>values);
        double calc_geometric(vector<double>values);
        double calc_harmonic(vector<double>values);
        double calc_centralMoment(vector<double>values,double x);
        double calc_standardDev(vector<double>values);
        double calc_quartile(vector<double>values,int setting);
        double calc_kurtosis(vector<double>values);
        double calc_slant(vector<double>values);
        double calc_counter(vector<double>values);
    private:
        double calc_round(double x);

};

#endif // CALC_H
