#ifndef FILEMNG_H
#define FILEMNG_H
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include "flower.h"

using namespace std;

struct record{
    double data;
    string name;
};
class fileMng
{
    public:
        fileMng(string path,int columns,vector<string>label,char sign);
        virtual ~fileMng();
    private:
        fstream file;
    protected:
        vector<flower> readCSV(string path,int columns,char sign);
        vector<record> prepearFlowers(int datatype);
        vector<flower> masterdata;
        vector<string> label;
};

#endif // FILEMNG_H
