#ifndef HISTOGRAM_H
#define HISTOGRAM_H
#include "fileMng.h"


class histogram : public fileMng
{
    public:
        histogram(string path,int columns,vector<string>label,char sign);
        virtual ~histogram();
        void create(int datatype);
    private:
        void printToPng(string source, string output);
        double findMin(vector<record>data);
        double findMax(vector<record>data);
        int nrofclasses();
};

#endif // HISTOGRAM_H
