#include "histogram.h"
#include "statistic.h"

using namespace std;

int main()
{
    vector<string>labels;
    int flag,columns,reqcolumn;
    double alpha;
    char sign;
    fstream file;
    file.open("cfg.txt");
    if(file.is_open()){
        string str;
        const char* tmp;
        int control = 0;
        while(getline(file,str)){
            if (control == 0){
                columns = atoi(str.c_str());
            }
            if (control == 1){
                reqcolumn = atoi(str.c_str());
            }
            if (control == 2){
                tmp = str.c_str();
                sign = *tmp;
            }
            if (control == 3){
                alpha = atof(str.c_str());
            }
            if (control >= 4){
                labels.push_back(str);
            }
            control++;
        }
    }
    else{
        cout<<"error - brak pliku konnfiguracyjnego\n";
        return 0;
    }
    file.close();

    cout<<"Progam zostanie uruchomiony z nastepujacymi parametrami:\nLiczba kolumn w pliku zrodlowym: "<<columns<<"\nNumer kolumny analizowanej: "<<reqcolumn<<"\nZnak rozdzielajacy: "<<sign<<"\nAlfa: "<<alpha<<"\nPodane etykiety:\n";
    for(int i = 0; i < labels.size(); i++){
        cout<<labels[i]<<"\n";
    }
    cout<<"Nacisnij ENTER aby kontynuowac\n";
    getchar();

    string path = "source.txt";

    system("cls");
    cout<<"Co chcesz zrobic ?\n";
    cout<<"[1] Rysowanie histogramu\n";
    cout<<"[2] Obliczanie statystyk\n";
    cout<<"[3] Testowanie hipotez\n";
    cin>>flag;
    system("cls");
    histogram hist(path,columns,labels,sign);
    statistic stat(path,columns,labels,sign);
    switch(flag){
        case 1:
            hist.create(reqcolumn);
            break;
        case 2:
            stat.arithmetic(reqcolumn);
            stat.geometric(reqcolumn);
            stat.harmonic(reqcolumn);
            stat.dominant(reqcolumn);
            stat.centralMoment(reqcolumn,1);
            stat.centralMoment(reqcolumn,3);
            stat.median(reqcolumn);
            stat.variance(reqcolumn);
            stat.standardDeviation(reqcolumn);
            stat.quartile(reqcolumn,3);
            stat.quartile(reqcolumn,1);
            stat.kurtosis(reqcolumn);
            stat.slant(reqcolumn);
            stat.counter(reqcolumn);
            break;
        case 3:
            stat.tstudent(reqcolumn,alpha);
            break;
    }


    cout<<"\n\nNacisnij dowolny klawisz by zakonczyc\n";



    getchar();
    return 0;
}
