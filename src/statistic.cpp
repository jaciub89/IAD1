#include "statistic.h"

statistic::statistic(string path,int columns,vector<string>label,char sign) : fileMng(path,columns,label,sign)
{

}

statistic::~statistic()
{
    //dtor
}
void statistic::arithmetic(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nSrednia arytmetyczna \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_arithmetic(splitData(values,label[i]))<<"\n";
    }
}

void statistic::geometric(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nSrednia geometryczna\n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_geometric(splitData(values,label[i]))<<"\n";
    }
}

void statistic::harmonic(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nSrednia harmoniczna \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_harmonic(splitData(values,label[i]))<<"\n";
    }
}

void statistic::dominant(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nDominanta \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_dominant(splitData(values,label[i]))<<"\n";
    }
}

void statistic::centralMoment(int datatype,double x){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nMoment centralny "<<x<<" rzedu \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_centralMoment(splitData(values,label[i]),x)<<"\n";
    }
}

void statistic::median(int datatype){

    vector<record>values = prepearFlowers(datatype);
    cout<<"\nMediana \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_median(splitData(values,label[i]))<<"\n";
    }
}

vector<double> statistic::splitData(vector<record>values,string str){
    vector<double> result;
    for(int i = 0; i < values.size();i++){
        if(values[i].name.compare(str) == 0){
            result.push_back(values[i].data);
        }
    }
    return result;
}

void statistic::variance(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nWariancja \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_centralMoment(splitData(values,label[i]),2)<<"\n";
    }
}
void statistic::standardDeviation(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nOdchylenie standardowe \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_standardDev(splitData(values,label[i]))<<"\n";
    }
}
void statistic::quartile(int datatype,int n){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nKwartyl Q"<<n<<"\n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_quartile(splitData(values,label[i]),n)<<"\n";
    }
}
void statistic::kurtosis(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nKurtoza \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_kurtosis(splitData(values,label[i]))<<"\n";
    }
}
void statistic::slant(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nWspolczynnik asymetrii \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_slant(splitData(values,label[i]))<<"\n";
    }
}
void statistic::counter(int datatype){
    vector<record>values = prepearFlowers(datatype);
    cout<<"\nLiczba wystapien \n";
    for(int i = 0; i < label.size();i++){
        cout<<label[i]<<" "<<calc_counter(splitData(values,label[i]))<<"\n";
    }
}
void statistic::tstudent(int datatype, double alpha){
    vector<record>values = prepearFlowers(datatype);
    double avg1, avg2, stdev1, stdev2,n1,n2,s1,s2,z,t,df;
    for(int i = 0; i <label.size();i++){
        if( i == 0){
            avg1 = calc_arithmetic(splitData(values,label[i]));
            stdev1 = calc_standardDev(splitData(values,label[i]));
            n1 = calc_counter(splitData(values,label[i]));
            s1 = pow(stdev1,2);
        }
        if( i == 1){
            avg2 = calc_arithmetic(splitData(values,label[i]));
            stdev2 = calc_standardDev(splitData(values,label[i]));
            n2 = calc_counter(splitData(values,label[i]));
            s2 = pow(stdev2,2);
        }
    }
    z = (avg1 - avg2) / ( sqrt((stdev1/n1) + (stdev2/n2)));
    z = -z;
    double norm = 0.5*(1+erf(z / sqrt(2)));
    df = (pow((pow(stdev1,2) / n1) + (pow(stdev2,2) / n2),2)) / ((pow(pow(stdev1,2) / n1,2) / (n1 - 1)) + (pow(pow(stdev2,2) / n2,2) / (n2 - 1)));
    t = (avg1 - avg2) / (sqrt(((((n1-1) * s1)  + ((n2-1) * s2)) / (n1+n2-2)) * ( (1/n1) + (1/n2) )));
    cout<<"\nsrednia 1: "<<avg1<<"\n";
    cout<<"\nsrednia 2: "<<avg2<<"\n";
    cout<<"\nodchylenie standardowe 1: "<<stdev1<<"\n";
    cout<<"\nodchylenie standardowe 2: "<<stdev2<<"\n";
    cout<<"\nn 1: "<<n1<<"\n";
    cout<<"\nn 2: "<<n2<<"\n";
    cout<<"\nWartosc statystyczna Z: "<<fabs(z)<<"\n";
    cout<<"\nP-value: "<<2*norm<<"\n";
    cout<<"\nLiczba stopni swobody: "<<df<<"\n";
    if ( t >= t_studentTab(df,alpha)){
        cout<<"\nT-Student: H0 odrzucona, potwierdzono H1\n";
    }
    else{
        cout<<"\nT-Student: H0 potwierdzona\n";
    }
    if ( (2*norm) < alpha){
        cout<<"\nP-value: H0 odrzucona, potwierdzono H1\n";
    }
    else{
        cout<<"\nP=value: H0 potwierdzona\n";
    }
}
int statistic::findDf(int df, double tab[][2]){
    for(int i = 0; i < 79 ; i++){
        double d1 = df - tab[i][1];
        double d2 = df - tab[i+1][1];

        if(fabs(d1) < fabs(d2)){
            return tab[i][0];
        }
    }
}
int statistic::findAlpha(double alpha, double tab[][2]){
    for(int i = 0; i < 8 ; i++){
        if(alpha == tab[i][1]){
            return tab[i][0];
        }
    }
}
double statistic::t_studentTab(int df,double alpha){
    double t_student[81][8] =
    {
        3.07768,6.31375,12.7062,15.8945,31.8205,63.6568,318.306,636.627,
        1.88562,2.91999,4.30265,4.84873,6.96456,9.92484,22.3272,31.5990,
        1.63774,2.35336,3.18245,3.48191,4.54070,5.84091,10.2145,12.9240,
        1.53321,2.13185,2.77644,2.99853,3.74695,4.60409,7.17318,8.61031,
        1.47588,2.01505,2.57058,2.75651,3.36493,4.03214,5.89344,6.86884,
        1.43976,1.94318,2.44691,2.61224,3.14267,3.70743,5.20763,5.95880,
        1.41492,1.89458,2.36462,2.51675,2.99795,3.49948,4.78528,5.40787,
        1.39682,1.85955,2.30600,2.44898,2.89646,3.35539,4.50079,5.04130,
        1.38303,1.83311,2.26216,2.39844,2.82144,3.24984,4.29681,4.78092,
        1.37218,1.81246,2.22814,2.35931,2.76377,3.16927,4.14370,4.58691,
        1.36343,1.79588,2.20099,2.32814,2.71808,3.10581,4.02470,4.43697,
        1.35622,1.78229,2.17881,2.30272,2.68100,3.05454,3.92963,4.31779,
        1.35017,1.77093,2.16037,2.28160,2.65031,3.01228,3.85198,4.22083,
        1.34503,1.76131,2.14479,2.26378,2.62449,2.97684,3.78739,4.14045,
        1.34061,1.75305,2.13145,2.24854,2.60248,2.94671,3.73283,4.07276,
        1.33676,1.74588,2.11991,2.23536,2.58349,2.92078,3.68615,4.01500,
        1.33338,1.73961,2.10982,2.22385,2.56693,2.89823,3.64576,3.96512,
        1.33039,1.73406,2.10092,2.21370,2.55238,2.87844,3.61048,3.92164,
        1.32773,1.72913,2.09302,2.20470,2.53948,2.86094,3.57940,3.88341,
        1.32534,1.72472,2.08596,2.19666,2.52798,2.84534,3.55181,3.84952,
        1.32319,1.72074,2.07961,2.18943,2.51765,2.83136,3.52715,3.81927,
        1.32124,1.71714,2.07387,2.18289,2.50832,2.81876,3.50499,3.79214,
        1.31946,1.71387,2.06866,2.17696,2.49987,2.80734,3.48496,3.76762,
        1.31784,1.71088,2.06390,2.17154,2.49216,2.79694,3.46678,3.74539,
        1.31635,1.70814,2.05954,2.16659,2.48511,2.78744,3.45019,3.72514,
        1.31497,1.70562,2.05553,2.16203,2.47863,2.77871,3.43500,3.70660,
        1.31370,1.70329,2.05183,2.15783,2.47266,2.77068,3.42103,3.68959,
        1.31253,1.70113,2.04841,2.15393,2.46714,2.76326,3.40816,3.67391,
        1.31143,1.69913,2.04523,2.15033,2.46202,2.75639,3.39624,3.65941,
        1.31041,1.69726,2.04227,2.14697,2.45726,2.75000,3.38519,3.64596,
        1.30946,1.69552,2.03951,2.14383,2.45282,2.74404,3.37490,3.63345,
        1.30857,1.69389,2.03693,2.14090,2.44868,2.73848,3.36531,3.62180,
        1.30774,1.69236,2.03452,2.13816,2.44479,2.73328,3.35634,3.61091,
        1.30695,1.69092,2.03224,2.13558,2.44115,2.72840,3.34793,3.60072,
        1.30621,1.68957,2.03011,2.13316,2.43772,2.72381,3.34004,3.59115,
        1.30551,1.68830,2.02809,2.13087,2.43449,2.71948,3.33262,3.58215,
        1.30485,1.68709,2.02619,2.12871,2.43145,2.71541,3.32563,3.57367,
        1.30423,1.68595,2.02439,2.12667,2.42857,2.71156,3.31903,3.56568,
        1.30364,1.68488,2.02269,2.12474,2.42584,2.70791,3.31279,3.55811,
        1.30308,1.68385,2.02108,2.12291,2.42326,2.70446,3.30688,3.55096,
        1.30254,1.68288,2.01954,2.12117,2.42080,2.70118,3.30127,3.54418,
        1.30204,1.68195,2.01808,2.11952,2.41847,2.69807,3.29595,3.53774,
        1.30155,1.68107,2.01669,2.11794,2.41625,2.69510,3.29089,3.53162,
        1.30109,1.68023,2.01537,2.11644,2.41413,2.69228,3.28607,3.52580,
        1.30065,1.67943,2.01410,2.11500,2.41212,2.68959,3.28148,3.52025,
        1.30023,1.67866,2.01290,2.11364,2.41019,2.68701,3.27710,3.51496,
        1.29982,1.67793,2.01174,2.11233,2.40835,2.68456,3.27291,3.50990,
        1.29944,1.67722,2.01063,2.11107,2.40658,2.68220,3.26891,3.50507,
        1.29907,1.67655,2.00958,2.10987,2.40489,2.67995,3.26508,3.50045,
        1.29871,1.67590,2.00856,2.10872,2.40327,2.67779,3.26141,3.49601,
        1.29713,1.67303,2.00404,2.10361,2.39608,2.66822,3.24515,3.47640,
        1.29582,1.67065,2.00030,2.09936,2.39012,2.66028,3.23171,3.46020,
        1.29471,1.66864,1.99714,2.09578,2.38510,2.65360,3.22042,3.44659,
        1.29376,1.66691,1.99444,2.09273,2.38081,2.64790,3.21079,3.43502,
        1.29294,1.66543,1.99210,2.09008,2.37710,2.64298,3.20249,3.42503,
        1.29222,1.66412,1.99006,2.08778,2.37387,2.63869,3.19526,3.41634,
        1.29159,1.66298,1.98827,2.08574,2.37102,2.63491,3.18890,3.40870,
        1.29103,1.66196,1.98667,2.08394,2.36850,2.63157,3.18327,3.40194,
        1.29053,1.66105,1.98525,2.08233,2.36624,2.62858,3.17825,3.39590,
        1.29007,1.66023,1.98397,2.08088,2.36422,2.62589,3.17374,3.39049,
        1.28930,1.65882,1.98177,2.07839,2.36073,2.62127,3.16598,3.38118,
        1.28865,1.65765,1.97993,2.07631,2.35782,2.61742,3.15954,3.37346,
        1.28810,1.65666,1.97838,2.07456,2.35537,2.61418,3.15411,3.36695,
        1.28763,1.65581,1.97705,2.07306,2.35328,2.61140,3.14946,3.36137,
        1.28722,1.65508,1.97591,2.07176,2.35146,2.60900,3.14545,3.35657,
        1.28687,1.65443,1.97490,2.07063,2.34988,2.60691,3.14195,3.35237,
        1.28655,1.65387,1.97402,2.06963,2.34848,2.60506,3.13886,3.34868,
        1.28627,1.65336,1.97323,2.06874,2.34724,2.60342,3.13612,3.34540,
        1.28602,1.65291,1.97253,2.06794,2.34613,2.60195,3.13368,3.34246,
        1.28580,1.65251,1.97190,2.06723,2.34514,2.60063,3.13148,3.33983,
        1.28560,1.65214,1.97132,2.06658,2.34424,2.59944,3.12949,3.33746,
        1.28541,1.65181,1.97081,2.06600,2.34342,2.59836,3.12769,3.33530,
        1.28524,1.65151,1.97033,2.06546,2.34267,2.59737,3.12604,3.33333,
        1.28509,1.65123,1.96990,2.06497,2.34199,2.59647,3.12454,3.33153,
        1.28495,1.65097,1.96950,2.06452,2.34136,2.59564,3.12315,3.32987,
        1.28482,1.65074,1.96913,2.06410,2.34078,2.59487,3.12188,3.32834,
        1.28469,1.65052,1.96879,2.06372,2.34024,2.59416,3.12069,3.32692,
        1.28458,1.65031,1.96847,2.06336,2.33974,2.59350,3.11960,3.32561,
        1.28448,1.65013,1.96818,2.06303,2.33928,2.59289,3.11857,3.32439,
        1.28438,1.64995,1.96790,2.06272,2.33884,2.59232,3.11762,3.32326,
        1.28155,1.64485,1.95996,2.05375,2.32635,2.57583,3.09023,3.29053
    };
    double t_df[80][2] =
    {
        0,1,
        1,2,
        2,3,
        3,4,
        4,5,
        5,6,
        6,7,
        7,8,
        8,9,
        9,10,
        10,11,
        11,12,
        12,13,
        13,14,
        14,15,
        15,16,
        16,17,
        17,18,
        18,19,
        19,20,
        20,21,
        21,22,
        22,23,
        23,24,
        24,25,
        25,26,
        26,27,
        27,28,
        28,29,
        29,30,
        30,31,
        31,32,
        32,33,
        33,34,
        34,35,
        35,36,
        36,37,
        37,38,
        38,39,
        39,40,
        40,41,
        41,42,
        42,43,
        43,44,
        44,45,
        45,46,
        46,47,
        47,48,
        48,49,
        49,50,
        50,55,
        51,60,
        52,65,
        53,70,
        54,75,
        55,80,
        56,85,
        57,90,
        58,95,
        59,100,
        60,110,
        61,120,
        62,130,
        63,140,
        64,150,
        65,160,
        66,170,
        67,180,
        68,190,
        69,200,
        70,210,
        71,220,
        72,230,
        73,240,
        74,250,
        75,260,
        76,270,
        77,280,
        78,290,
        79,300
    };
    double t_alpha[8][2] =
    {
        0,0.2,
        1,0.1,
        2,0.05,
        3,0.04,
        4,0.02,
        5,0.01,
        6,0.002,
        7,0.001
    };
    int row;
    int column = findAlpha(alpha,t_alpha);
    if (df >= 301){
        row = 80;
    }
    else{
        row = findDf(df,t_df);
    }
    return t_student[row][column];
}







