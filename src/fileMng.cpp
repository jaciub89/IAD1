#include "fileMng.h"

fileMng::fileMng(string path,int columns,vector<string>label,char sign)
{
    masterdata = readCSV(path,columns,sign);
    this->label = label;
}

fileMng::~fileMng()
{

}
vector<flower> fileMng::readCSV(string path, int columns,char sign){
    file.open(path.c_str());
    vector<flower>tab;
    string data,tmp,type;
    if(file.is_open()){
        while(getline(file,data,sign)){
                flower newFlower(columns);
                newFlower.tab[0] = atof(data.c_str());
                for(int i = 1; i < (columns -1 ); i++){
                    getline(file, tmp, sign) ;
                    newFlower.tab[i] = atof(tmp.c_str());
                }
                getline(file, type);
                newFlower.type = type;
                if (newFlower.tab[0] != 0){
                    tab.push_back(newFlower);
                }
        }
        file.close();
    }
    else{
        cout<<"error - blad otwarcia: " + path + "\n";
    }

    return tab;

}

vector<record> fileMng::prepearFlowers(int datatype){
    vector<record>values;
    for(int i = 0; i < masterdata.size() ; i++){
        record newRecord;
        newRecord.data = masterdata[i].tab[datatype - 1];
        newRecord.name = masterdata[i].type;
        values.push_back(newRecord);
    }
    return values;
}
