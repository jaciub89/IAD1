#include "histogram.h"
#include "gnuplot_i.hpp"
#include <cmath>
histogram::histogram(string path,int columns,vector<string>label,char sign) :fileMng(path,columns,label,sign)
{

}

histogram::~histogram()
{
    //dtor
}
void histogram::create(int datatype){
    vector<record>values = prepearFlowers(datatype);
    int *counter = new int[label.size()];
    for(int i = 0 ; i < label.size();i++){
        counter[i] = 0;
    }
    ofstream file("temp.txt");
    if (file.is_open()){
        file<<"Dane\t";
        for(int i = 0;i < label.size(); i++){
            file<<label[i]<<"\t";
        }
        file<<"\n";

        double minvalue = findMin(values);

        int classes = nrofclasses();
        float interval = (findMax(values) - findMin(values)) / classes;

        for(int i = 0; i < classes ;i++){

            float tmpmin = float(minvalue);
            float tmpmax;

            if ((classes - 1) == i){
                tmpmax = tmpmin + interval;
            }
            else{
                tmpmax = tmpmin + (interval - 0.01);
            }

            tmpmax = roundf(tmpmax * 100) / 100;
            tmpmin = roundf(tmpmin * 100) / 100;

            file<<tmpmin<<"-"<<tmpmax<<"\t";
            for(int j =0; j < values.size();j++){
                    float value  = float(values[j].data);
                    value = roundf(value * 100) / 100;
                    if( (value >= tmpmin) && ( value <= tmpmax)){
                        for(int k = 0; k <label.size(); k++){
                            if(values[j].name.compare(label[k]) == 0){
                                counter[k]++;
                            }
                        }
                    }
            }
            for(int j = 0; j < label.size(); j++){
                file<<counter[j]<<"\t";
                counter[j] = 0;
            }
            file<<"\n";
            minvalue += interval;
        }
        file.close();
    }
    else{
        cout<<"error - nie udalo sie utworzyc pliku\n";
    }
    string out = "wykres.png";
    printToPng("temp.txt",out);
}
double histogram::findMin(vector<record>data){
    double res = data[0].data;
    for(int i = 0 ; i < data.size(); i++)
    {
        if(res > data[i].data){
            res = data[i].data;
        }
    }
    return res;
}

double histogram::findMax(vector<record>data){
    double res = data[0].data;
    for(int i = 0 ; i < data.size(); i++)
    {
        if(res < data[i].data){
            res = data[i].data;
        }
    }
    return res;
}

int histogram::nrofclasses(){
    return sqrt(masterdata.size());
}

void histogram::printToPng(string source,string output){
    Gnuplot::set_GNUPlotPath( "C:\\gnuplot\\bin");
    Gnuplot main_plot;
    main_plot.cmd( "set title '" + output + "'" );
    main_plot.cmd( "set term png ");
    main_plot.cmd( "set output '" + output + "'");
    main_plot.cmd( "set auto x" );
    main_plot.cmd( "set auto y" );
    main_plot.cmd( "set style data histogram" );
    main_plot.cmd( "set style fill solid border -1" );
    main_plot.cmd( "set boxwidth 0.9" );
    main_plot.cmd( "set xtic rotate by -45 scale 0" );
    main_plot.cmd( "plot '" + source + "' using 2:xtic(1) ti col, '' u 3 ti col, '' u 4 ti col" );
}
