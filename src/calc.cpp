#include "calc.h"

calc::calc()
{
    //ctor
}

calc::~calc()
{
    //dtor
}
double calc::calc_arithmetic(vector<double>values){
    double sum = 0;
    double counter = 0;
    for(int i = 0; i < values.size(); i++){
        sum += values[i];
        counter++;
    }
    return calc_round(sum/counter);
}
double calc::calc_geometric(vector<double>values){
    double counter = 0;
    double product = 1;
    for(int i = 0; i < values.size();i++){
        product *= values[i];
        counter++;
    }
    return calc_round(pow(product,1/counter));
}
double calc::calc_harmonic(vector<double>values){
    double sum = 0;
    double counter = 0;
    for(int i =0; i < values.size(); i++){
        sum += (1/values[i]);
        counter++;
    }
    return calc_round(counter/sum);
}
double calc::calc_dominant(vector<double>values){

    vector<double>founded;
    vector<int>counter;

    bool notFounded;
    int pos = 0;

    for(int i = 0; i < values.size(); i++){
        notFounded = true;
        for(int j = 0 ; j < founded.size(); j++){
            if(values[i] == founded[j]){
                notFounded = false;
                pos = j;
                break;
            }
        }

        if (notFounded){
            founded.push_back(values[i]);
            counter.push_back(1);
        }
        else{
            counter[pos]++;
        }

    }
    int res = counter[0];
    for(int i = 0 ; i < counter.size(); i++)
    {
        if(res < counter[i]){
            res = counter[i];
            pos = i;
        }
    }

    return calc_round(founded[pos]);
}
double calc::calc_centralMoment(vector<double>values,double x){
    double avg = calc_arithmetic(values);
    double sum = 0;
    double counter = 0;
    for (int i =0 ; i < values.size();i++){
        sum += pow((values[i] - avg),x);
        counter++;
    }
    return calc_round(sum/counter);
}
double calc::calc_median(vector<double>values){
    sort(values.begin(),values.end(), sortingStruct());
    int n = values.size();
    if((n%2) == 0){
        return calc_round(((values[(n/2)] + values[((n/2)+1)]) / 2));
    }
    else{
        return calc_round(values[((n+1)/2)]);
    }

}
double calc::calc_round(double x){
    return roundf(x * 1000) / 1000;
}
double calc::calc_standardDev(vector<double>values){
    return calc_round(sqrt(calc_centralMoment(values,2)));
}
double calc::calc_quartile(vector<double>values, int setting){
    sort(values.begin(),values.end(), sortingStruct());
    int n = values.size();
    int pos;
    if (setting == 1){
        pos = 0.25 * (n + 1);
    }
    if (setting == 3){
        pos = 0.75 * (n + 1);
    }
    return calc_round(values[pos]);
}
double calc::calc_kurtosis(vector<double>values){
    return calc_round( (calc_centralMoment(values,4) / pow(calc_standardDev(values),4)) - 3);
}
double calc::calc_slant(vector<double>values){
    double avg = calc_arithmetic(values);
    double mode = calc_dominant(values);
    double stddev = calc_standardDev(values);

    return calc_round((avg - mode) / stddev);
}
double calc::calc_counter(vector<double>values){
    return values.size();
}
